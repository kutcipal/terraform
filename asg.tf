resource "aws_autoscaling_group" "test_asg" {
  lifecycle { create_before_destroy = true }

  # spread the app instances across the availability zones
  availability_zones = ["${split(",", var.az)}"]

  # interpolate the LC into the ASG name so it always forces an update
  name = "asg-app - ${aws_launch_configuration.test_lc.name}"
  max_size = 1
  min_size = 1
  wait_for_elb_capacity = 1
  desired_capacity = 1
  health_check_grace_period = 300
  health_check_type = "ELB"
  launch_configuration = "${aws_launch_configuration.test_lc.id}"
  load_balancers = ["${aws_elb.test_elb.id}"]
  vpc_zone_identifier = ["${aws_subnet.tf_public_1.id}", "${aws_subnet.tf_public_2.id}"]

  tag {
    key = "Name"
    value = "test_asg"
    propagate_at_launch = true
  }
}

resource "aws_launch_configuration" "test_lc" {
  lifecycle { create_before_destroy = true }

  image_id = "${var.web_amis}"
  instance_type = "t2.micro"

  security_groups = ["${aws_security_group.test_elb.id}"]

}
