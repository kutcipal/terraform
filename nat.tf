resource "aws_instance" "nat" {
  ami = "${lookup(var.nat_amis, var.aws_region)}"
  key_name = "${aws_key_pair.testing.key_name}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.tf_public_1.id}"
  vpc_security_group_ids = ["${aws_default_security_group.default.id}","${aws_security_group.test_nat.id}"]
  source_dest_check = false
  tags = { 
    Name = "test-nat"
    env = "test"
    provision = "terraform"
  }
}
