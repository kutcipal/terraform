/* Setup AWS provider */
provider "aws" {
  shared_credentials_file = "/home/vagrant/.aws/credentials"
  region      = "${var.aws_region}"
}

/* Define vpc */
resource "aws_vpc" "terraform_test" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  tags {
    Name = "terraform_vpc_test"
  }
}


