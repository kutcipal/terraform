resource "aws_instance" "test_web" {
  ami = "${var.web_amis}"
  instance_type = "t2.micro"
  depends_on = ["aws_elb.test_elb"]
  subnet_id = "${aws_subnet.tf_public_1.id}"
  vpc_security_group_ids = ["${aws_security_group.test_web.id}"]
  key_name = "${aws_key_pair.testing.key_name}"
  source_dest_check = true
  tags = { 
    Name = "test-web"
    env = "test"
    provision = "terraform"
  }
}

resource "aws_elb_attachment" "test_elb" {
  elb      = "${aws_elb.test_elb.id}"
  instance = "${aws_instance.test_web.id}"
}

