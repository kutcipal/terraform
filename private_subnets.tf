# Create subnets
resource "aws_subnet" "tf_private_1" {
  vpc_id = "${aws_vpc.terraform_test.id}"
  cidr_block = "${var.private_subnet_1}"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = false
  depends_on = ["aws_instance.nat"]
  tags {
    Name = "private_az1"
  }
}

resource "aws_subnet" "tf_private_2" {
  vpc_id = "${aws_vpc.terraform_test.id}"
  cidr_block = "${var.private_subnet_2}"
  availability_zone = "us-east-1b"
  map_public_ip_on_launch = false
  depends_on = ["aws_instance.nat"]
  tags {
    Name = "private_az2"
  }
}

# Create route table - NAT instance
resource "aws_route_table" "tf_private" {
  vpc_id = "${aws_vpc.terraform_test.id}"
  route {
    cidr_block = "0.0.0.0/0"
    instance_id = "${aws_instance.nat.id}"
  }
}

# Associate route table with subnets
resource "aws_route_table_association" "tf_private_1" {
  subnet_id = "${aws_subnet.tf_private_1.id}"
  route_table_id = "${aws_route_table.tf_private.id}"
}

resource "aws_route_table_association" "tf_private_2" {
  subnet_id = "${aws_subnet.tf_private_2.id}"
  route_table_id = "${aws_route_table.tf_private.id}"
}

