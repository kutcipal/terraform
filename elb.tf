resource "aws_elb" "test_elb" {
  name = "test-elb"

  listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }

  health_check {
    healthy_threshold = 3
    unhealthy_threshold = 2
    timeout = 10
    target = "HTTP:80/"
    interval = 30
  }

  cross_zone_load_balancing = true
  idle_timeout = 60
  subnets         = ["${aws_subnet.tf_public_1.id}", "${aws_subnet.tf_public_2.id}"]
  security_groups = ["${aws_security_group.test_elb.id}"]

  tags {
    Name = "test-elb"
  }
}

