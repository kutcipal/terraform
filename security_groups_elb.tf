/* Security group for the ELB */
resource "aws_security_group" "test_elb" {
  name = "test-elb"
  description = "Security group for elb"
  vpc_id = "${aws_vpc.terraform_test.id}"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "test-elb"
  }
}

