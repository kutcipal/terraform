resource "aws_key_pair" "testing" {
  key_name   = "testing"
  public_key = "${file("ssh_keys/testing.pub")}"
}
