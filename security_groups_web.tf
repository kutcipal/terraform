/* Security group for the web */
resource "aws_security_group" "test_web" {
  name = "test-web"
  description = "Security group for web"
  vpc_id = "${aws_vpc.terraform_test.id}"
  depends_on = ["aws_security_group.test_elb"]

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    security_groups = ["${aws_security_group.test_nat.id}"]
  }
  
  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    security_groups = ["${aws_security_group.test_elb.id}"]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "test-web"
  }
}

