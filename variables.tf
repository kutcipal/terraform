variable "aws_region"     { 
  description = "AWS region"
  default     = "us-east-1" 
}

variable "az"  {
  description = "AZs to use"
  default = "us-east-1a,us-east-1b"
}

variable "vpc_cidr" {
  description = "CIDR for VPC"
  default     = "10.10.0.0/16"
}

variable "public_subnet_1" {
  description = "public subnet_1"
  default     = "10.10.0.0/24"
}

variable "public_subnet_2" {
  description = "public subnet_2"
  default     = "10.10.1.0/24"
}

variable "private_subnet_1" {
  description = "private subnet_1"
  default     = "10.10.2.0/24"
}

variable "private_subnet_2" {
  description = "private subnet_2"
  default     = "10.10.3.0/24"
}

/* amazon linux NAT AMI */
variable "nat_amis" {
  description = "NAT AMI"
  default = {
    us-west-1 = "ami-004b0f60"
    us-east-1 = "ami-258e1f33"
  }
}

/* Packer built AMIs */
variable "web_amis" {
  description = "Packer built AMI"
}

