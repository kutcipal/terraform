# Create subnets
resource "aws_subnet" "tf_public_1" {
  vpc_id = "${aws_vpc.terraform_test.id}"
  cidr_block = "${var.public_subnet_1}"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = true
  tags {
    Name = "public_az1"
  }
}

resource "aws_subnet" "tf_public_2" {
  vpc_id = "${aws_vpc.terraform_test.id}"
  cidr_block = "${var.public_subnet_2}"
  availability_zone = "us-east-1b"
  map_public_ip_on_launch = true
  tags {
    Name = "public_az2"
  }
}

# Create Internet gateway
resource "aws_internet_gateway" "tf_public" {
  vpc_id = "${aws_vpc.terraform_test.id}"
}

# Create route table
resource "aws_route_table" "tf_public" {
  vpc_id = "${aws_vpc.terraform_test.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.tf_public.id}"
  }
}

# Associate route table with subnets
resource "aws_route_table_association" "tf_public_1" {
  subnet_id = "${aws_subnet.tf_public_1.id}"
  route_table_id = "${aws_route_table.tf_public.id}"
}

resource "aws_route_table_association" "tf_public_2" {
  subnet_id = "${aws_subnet.tf_public_2.id}"
  route_table_id = "${aws_route_table.tf_public.id}"
}
