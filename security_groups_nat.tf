/* Security group for the NAT */
resource "aws_security_group" "test_nat" {
  name = "test-nat"
  description = "Security group for nat instances"
  vpc_id = "${aws_vpc.terraform_test.id}"
  
  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags { 
    Name = "test-nat" 
  }
}

